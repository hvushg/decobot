from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from .storage import Storage
import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)


class Bot(object):
    def __init__(self, token):
        self.token = token
        self.updater = Updater(token)
        self.dis = self.updater.dispatcher
        self.storage = Storage(max_number_of_users=100)
        self.logger = logging.getLogger(self.__class__.__name__)

    def setup(self):
        self.build_handlers()
        self.logger.info("setup")

    def build_handlers(self):
        pass

    def run(self, **pooling_kwargs):
        self.updater.start_polling(**pooling_kwargs)
        self.updater.idle()

    def start(self):
        self.setup()
        try:
            self.run()
        except:
            self.finish()

    def finish(self):
        self.logger.info("finish")

    def errors_handler(bot, update, error):
        print(type(error), error)

    def add_handler(self, handler, **kwargs):
        self.dis.add_handler(handler, **kwargs)
