from telegram.ext import BaseFilter


class NewMember(BaseFilter):
    def filter(self, message):
        if not message.new_chat_members:
            return False
        return True


class LessMember(BaseFilter):
    def filter(self, message):
        if not message.left_chat_member:
            return False
        return True


class NewUser(BaseFilter):
    def __init__(self, users_ids):
        self.users_ids = users_ids

    def filter(self, message):
        return message.from_user.id not in self.users_ids


class OldUser(BaseFilter):
    def __init__(self, users_ids):
        self.users_ids = users_ids

    def filter(self, message):
        return message.from_user.id in self.users_ids


class SpamFilter(BaseFilter):
    def __init__(self, message_history, number_of_messages=20):
        self.message_history = message_history
        self.number_of_messages = number_of_messages

    def filter(self, message):
        count_user_show = 0
        for message_h in self.message_history[-self.number_of_messages*2:]:
            if message_h.from_user.id == message.from_user.id:
                count_user_show += 1
        if count_user_show >= self.number_of_messages:
            return True
        return False

