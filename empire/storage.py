from time import time


class Storage(object):
    def __init__(self, max_number_of_users):
        self.max_number_of_users = max_number_of_users
        self.cuurent_number_of_users = 0
        self.users_data = {}
        self.global_data = {}

    def get_user_data(self, user_id):
        if user_id not in self.users_data:
            self.users_data[user_id] = {"user_id": user_id}
            self.cuurent_number_of_users += 1
        if self.cuurent_number_of_users == self.max_number_of_users:
            self.clean_users()
        if self.cuurent_number_of_users >= self.max_number_of_users:
            raise Exception("Storage is full can't add another user.")

        user_data = self.users_data[user_id]
        user_data["lest_update"] = time()
        return user_data

    def clean_users(self):
        copy_user_data = self.users_data.copy()
        now_time = time()
        for user in copy_user_data:
            if now_time - user["lest_update"] > 100:
                self.users_data.pop(user["user_id"])
                self.cuurent_number_of_users -= 1

    def __getattr__(self, user_id):
        return self.get_user_data(user_id)

    def get_global_data(self):
        return self.global_data
