from telegram import ReplyKeyboardMarkup, InlineKeyboardButton, InlineKeyboardMarkup


class KeyboardHelper:
    @staticmethod
    def keyboard_from_list(lis, **kwargs):
        keyboard = [[i] for i in lis]
        return ReplyKeyboardMarkup(keyboard, **kwargs)

    @staticmethod
    def build_inline_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
        """
        :param buttons: a list of buttons.
        :param n_cols:  how many columns to show the butt,ons in
        :param header_buttons:  list of buttons appended to the beginning
        :param footer_buttons:  list of buttons added to the end
        :return: the menu
        """
        menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
        if header_buttons:
            menu.insert(0, header_buttons)
        if footer_buttons:
            menu.append(footer_buttons)
        return InlineKeyboardMarkup(menu)
