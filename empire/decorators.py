from functools import wraps
from warnings import warn
from telegram import ChatAction
from mwt import mwt
from . import __config__ as config

__all__ = ["Handlers", "Admins", "Group", "Helper",
           "send_typing_action", "send_upload_photo_action",
           "send_find_location_action", "send_record_audio_action",
           "send_record_video_action", "send_upload_audio_action",
           "send_upload_document_action", "send_upload_video_action"]


class Handlers:
    @staticmethod
    def get_user_data(func):
        @wraps(func)
        def wrapped(self, bot, update, *args, **kwargs):
            user_id = update.effective_user.id
            user_data = self.storage[user_id]
            return func(self, bot, update, user_data=user_data)
        return wrapped

    @staticmethod
    def send_action(action):
        """Sends `action` while processing func command."""
        def decorator(func):
            @wraps(func)
            def wrapped(self, bot, update, *args, **kwargs):
                bot.send_chat_action(chat_id=update.effective_message.chat_id, action=action)
                return func(self, bot, update,  *args, **kwargs)
            return wrapped
        return decorator

    @staticmethod
    def register_new_users(users):
        def decorator(func):
            @wraps(func)
            def wrapped(self, bot, update, *args, **kwargs):
                user = update.effective_user
                user_id = update.effective_user.id
                if user_id not in users:
                    users[user_id] = user
                return func(self, bot, update,  *args, **kwargs)
        return decorator

send_typing_action = Handlers.send_action(ChatAction.TYPING)
send_upload_photo_action = Handlers.send_action(ChatAction.UPLOAD_PHOTO)
send_find_location_action = Handlers.send_action(ChatAction.FIND_LOCATION)
send_record_audio_action = Handlers.send_action(ChatAction.RECORD_AUDIO)
send_record_video_action = Handlers.send_action(ChatAction.RECORD_VIDEO)
send_upload_audio_action = Handlers.send_action(ChatAction.UPLOAD_AUDIO)
send_upload_document_action = Handlers.send_action(ChatAction.UPLOAD_DOCUMENT)
send_upload_video_action = Handlers.send_action(ChatAction.UPLOAD_VIDEO)


class Group:
    @staticmethod
    def allowed_for_chat_admins(func):
        @wraps(func)
        def wrapped(self, bot, update, *args, **kwargs):
            if update.message.chat.all_members_are_administrators:
                warn("all chat members are administrators.\n everyone have access!\n@allowed_for_chat_admins")
            if update.effective_user.id in Helper.get_chat_admins(bot, update):
                return func(self, bot, update, *args, **kwargs)
        return wrapped


class Admins:
    @staticmethod
    def restricted(func):
        @wraps(func)
        def wrapped(self, bot, update, *args, **kwargs):
            user_id = update.effective_user.id
            if user_id not in config.LIST_OF_ADMINS:
                return Helper.not_allowed(bot, update, *args, **kwargs)
            return func(self, bot, update, *args, **kwargs)
        return wrapped

    @staticmethod
    def allowed_with_password(password, replay_message=True):
        def decorator(func):
            @wraps(func)
            def wrapped(self, bot, update, *args, **kwargs):
                if update.message.text != password and replay_message:
                    return Helper.not_allowed(bot, update)
                return func(self, bot, update, *args, **kwargs)
            return wrapped
        return decorator


class Helper:
    @staticmethod
    def not_allowed(bot, update, *args, **kwargs):
        update.message.reply_text("Unauthorized access denied!")

    @staticmethod
    @mwt(5*60)
    def get_chat_admins(bot, update, *args, **kwargs):
        return [admin.user.id for admin in bot.get_chat_administrators(update.message.chat.id)]

