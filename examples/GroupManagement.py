from empire.bot import Bot
from empire.decorators import Admins, ChatAction, Handlers, Group, Helper
from telegram.ext import CommandHandler, MessageHandler, ConversationHandler
from telegram.ext import Filters
from empire.Filters import SpamFilter, NewMember, LessMember


class GroupHandlers(object):
    def __init__(self, storage):
        self.storage = storage
        self.group_message_history = []
        self.number_messages_of_spam = 20

    def build_handlers(self):
        handlers = {}
        kick_user_handler = CommandHandler("kick",  self.kick_user, filters=Filters.reply & Filters.group)
        handlers["kick_user_handler"] = kick_user_handler

        warn_user_handler = CommandHandler("kick", self.kick_user, filters=Filters.reply & Filters.group)
        handlers["warn_user_handler"] = warn_user_handler

        spam_handler = MessageHandler(callback=self.spam_user, filters=SpamFilter(self.group_message_history,
                                                                                  self.number_messages_of_spam))
        handlers["spam_handler"] = spam_handler

        welcome_member = MessageHandler(callback=self.new_member, filters=NewMember())
        handlers["welcome_member"] = welcome_member

        member_left = MessageHandler(callback=self.left_member, filters=LessMember())
        handlers["member_left"] = member_left

    @Group.allowed_for_chat_admins
    def kick_user(self, bot, update):
        chat_id = update.message.chat.id
        user_id = update.message.reply_to_message.from_user.id
        bot.kick_chat_member(chat_id, user_id)

    @Group.allowed_for_chat_admins
    def warn_user(self, bot, update):
        user_id = update.message.reply_to_message.from_user.id

        user_data = self.storage[user_id]
        global_data = self.storage.get_global_data()

        warn_number = 0
        if "warn_number" in user_data:
            warn_number = user_data["warn_number"]
        user_data["warn_number"] = warn_number+1

        number_of_warnings = 3
        if "number_of_warnings" in global_data:
            number_of_warnings = global_data["number_of_warnings"]

        if number_of_warnings <= warn_number:
            bot.kick_chat_member(update.message.chat.id, user_id)

        if "chat_id" in user_data:
            chat_id = user_data["chat_id"]
            bot.send_message(chat_id, "your {} warning of {}".format(warn_number+1, number_of_warnings))

    def spam_user(self, bot, update):
        user_id = update.message.from_user.id
        bot.kick_chat_member(update.message.chat.id, )
        self.send_private_message(bot, user_id, "your spamming my group!")

    def send_private_message(self, bot, user_id, message):
        user_data = self.storage[user_id]

        if "chat_id" in user_data:
            chat_id = user_data["chat_id"]
            bot.send_message(chat_id, message)

    def new_member(self, bot, update):
        for member in update.message.new_chat_members:
            self.send_private_message(bot, member.id, "welcome to my group.")

    def left_member(self, bot, update):
        for member in update.message.left_chat_members:
            self.send_private_message(bot, member.id, "why do you left my group? please come back.")


class ManagementBot(Bot):
    def __init__(self, token):
        super().__init__(token)
        self.group_handlers = GroupHandlers(self.storage)

    def build_handlers(self):
        for name, handler in self.group_handlers.build_handlers().items():
            print("add handler:", name)
            self.add_handler(handler)



