from empire.bot import Bot
from empire.decorators import *
from telegram.ext import CommandHandler, MessageHandler, Filters

from time import sleep


class MyBot(Bot):
    def build_handlers(self):
        get_password_handler = MessageHandler(Filters.text, self.get_password_test)
        start_handler = CommandHandler("start", self.start_func)

        self.add_handler(get_password_handler)
        self.add_handler(start_handler)

    @Group.allowed_for_chat_admins
    def get_password_test(self, bot, update):
        update.message.reply_text("hello")

    @send_typing_action
    def start_func(self, bot, update):
        update.message.reply_text("hello")
        sleep(2)


def main():
    my_bot = MyBot("<ID>")
    my_bot.start()

if __name__ == '__main__':
    main()
