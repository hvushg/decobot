from empire.bot import Bot
from empire.Filters import NewUser, OldUser
from telegram.ext import CommandHandler
from telegram import InlineKeyboardButton

users = []


class RootBot(Bot):
    def build_handlers(self):
        new_members_start = CommandHandler(command="start", filters=NewUser(users), callback=self.new_start)
        old_members_start = CommandHandler(command="start", filters=OldUser(users), callback=self.old_start)

        self.add_handler(new_members_start)
        self.add_handler(old_members_start)

    def new_start(self, bot, update):
        update.message.reply_text("welcome!\niam see that is your fist time in my chat.\nhave a good time.")
        users.append(update.message.from_user.id)

    def old_start(self, bot, update):
        update.message.reply_text("welcome back!")
        InlineKeyboardButton(text="Our Technical Support Group", url=r"t.me\OurTechnicalSupportGroup")

def main():
    root_bot = RootBot("632163239:AAH7xMFaLVs6DFrwhQZo77E1VUmtkOZr9TU")
    root_bot.start()

main()
